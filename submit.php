<?php
/**
 * Created by IntelliJ IDEA.
 * User: ptah
 * Date: 28/11/2018
 * Time: 16:17
 */

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

//Load Composer's autoloader
require 'vendor/autoload.php';

$from = $_POST['email'];
$name = $_POST['name'];
$message = $_POST['message'];

$htmlBody = '<html style="margin: 0; padding: 0">
<link href="https://fonts.googleapis.com/css?family=Montserrat:300" rel="stylesheet">
<body style="margin: 0; padding: 0">
<div style="width: 100%; background: #518AC0;">
    <table border="0"  style="width: 100%; border: 1; max-width: 960px; margin:auto; padding:25px 20px">
        <tr>
            <td><img src="https://pontemventures.com/images/pontem-logo.png" width="80px" style="margin-right: 20px;"/> </td>
            <td style="font-size: 24px;  font-family: \'Montserrat\', Helvetica, Arial, sans-serif !important; color: #FFF;">Prospective Investor or Investee below</td>
        </tr>
    </table>
</div>
<div style="width: 100%;">
    <table border="0" style="width: 100%; border: 0; max-width: 960px; margin:auto; padding:50px 20px">
        <tr>
            <td style="font-size: 16px; font-family: \'Montserrat\', Helvetica, Arial, sans-serif !important; color: #282828; padding: 10px 0;"><strong>Name: </strong>'.$name[0].'</td>
        </tr>
        <tr>
            <td style="font-size: 16px; font-family: \'Montserrat\', Helvetica, Arial, sans-serif !important; color: #282828; padding: 10px 0;"><strong>Email: </strong>'.$from[0].'</td>
        </tr>
        <tr>
            <td style="font-size: 16px; font-family: \'Montserrat\', Helvetica, Arial, sans-serif !important; color: #282828; padding: 10px 0;"><strong>Message: </strong>'.$message.'</td>
        </tr>
    </table>
</div>
</body>
</html>';

$mail = new PHPMailer(true);                              // Passing `true` enables exceptions
try {
    //Server settings
    $mail->SMTPDebug = false;                                 // Enable verbose debug output
    $mail->isSMTP();                                      // Set mailer to use SMTP
    $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
    $mail->SMTPAuth = true;                               // Enable SMTP authentication
    $mail->Username = 'noreply@pontemventures.com';                 // SMTP username
    $mail->Password = 'pontem@123';                           // SMTP password
    $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
    $mail->Port = 587;                                    // TCP port to connect to

    //Recipients
    $mail->setFrom($from[0], $name[0]);
    $mail->addAddress('info@pontemventures.com', 'Pontem Ventures');     // Add a recipient
    $mail->addReplyTo($from[0], $name[0]);
    //Content
    $mail->isHTML(true);
    $mail->Subject = 'Pontem Ventures Online Request';
    $mail->Body    = $htmlBody;
    $mail->AltBody = $message;

    $mail->send();
    echo 'done';
} catch (Exception $e) {
    echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
}