$(document).on('ready', function () {
    $( ".navbar-toggler" ).on( "click", function() {
        $(".collapse").parent().parent().parent().css("cssText", "background-color: rgba(0, 0, 0, 1) !important;");
        $(".show").parent().parent().parent().css("cssText", "background-color: rgba(0, 0, 0, 0.7) !important;");
    });

    $("a").on('click', function(event) {
        if (this.hash !== "") {
            event.preventDefault();
            var hash = this.hash;
            $('html, body').animate({
                scrollTop: $(hash).offset().top - 100
            }, 800, function(){
               // window.location.hash = hash;
            });
        }
    });
    var committeeHeight = $("#committee-members").outerHeight()+"px";
    var negcommitteeHeight = "-"+$("#committee-members").outerHeight()+"px";

    $('#committee-members').css("margin-top", negcommitteeHeight );

    $("#committee-members").on('resize', function () {
        committeeHeight = $("#committee-members").outerHeight()+"px";
        negcommitteeHeight = "-"+$("#committee-members").outerHeight()+"px";

        $('#committee-members').css("margin-top", negcommitteeHeight );
    });


    $('#investment-committee').click(function() {
        $("#committee-members").animate({ 'margin-top': '0px' }, 1000);
        $("#investment-committee").animate({ 'opacity': '0' }, 1000);
    });
    $("#hide-investment-committee").click( function () {
        $("#committee-members").animate({ 'margin-top': negcommitteeHeight }, 1000);
        $("#investment-committee").animate({ 'opacity': '1' }, 1000);
    });

    $('#submitcontact').click(function() {
        $("#notification").animate({ 'margin-bottom': -50 }, 100);
        $('#name').removeClass('is-invalid');
        $('#email').removeClass('is-invalid');
        $('#message').removeClass('is-invalid');

        let var_name = validateName();
        let var_email = validateEmail();
        let var_message = validateMessage();

        if ( var_name == null ){
            $('#name').focus().addClass('is-invalid');
            return false;
        }

        if ( var_email == null ){
            $('#email').focus().addClass('is-invalid');
            return false;
        }

        if ( var_message == false ){
            $('#message').focus().addClass('is-invalid');
            return false;
        }

        //submit
        $.post('submit.php', {"name":var_name, "email":var_email, "message":var_message}, function (response) {
            if ( response == "done" ){
                $('#notification').html("<p class='text-center'>Your message was submitted successfully, we will be in touch.</p>");
                $("#notification").animate({ 'margin-bottom': 0 }, 500);
                $('#name').val("");
                $('#email').val("");
                $('#message').val("");
            }else{
                $('#notification').html("<p class='text-center'>Your message was submitted successfully, we will be in touch.</p>");
                $("#notification").animate({ 'margin-bottom': 0 }, 500);
            }
        });

        return true;
    });

    function validateName(){
        let name = $('#name').val();
        name = name.trim();
        return name.match(/^[a-zA-Z]+(([',. -][a-zA-Z ])?[a-zA-Z]*)*$/g) ;
    }

    function validateEmail(){
        let email = $('#email').val();
        email = email.trim();
        return email.match(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/igm);
    }

    function validateMessage(){
        let message = $('#message').val();
        if ( message != "" ){
            return message;
        }else{
            return false;
        }
    }
});